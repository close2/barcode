#include <stdio.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_log.h"
#include "driver/rmt.h"

#include "ean13.h"

const uint16_t delaybase = 50; // microseconds

static const char *BARCODE_TAG = "Barcode Tx";

#define RMT_TX_CHANNEL RMT_CHANNEL_0
#define RMT_TX_GPIO 18

static void IRAM_ATTR bool_to_rmt ( const void* src, rmt_item32_t* pdest, size_t src_size,
                                    size_t wanted_num, size_t* translated_size,
                                    size_t* item_num )
{
    if ( src == NULL || pdest == NULL ) {
        *translated_size = 0;
        *item_num = 0;
        return;
    }
    const rmt_item32_t off = {{{ 0, 1, delaybase, 0 }}}; //Logical 0
    const rmt_item32_t on = {{{ delaybase, 1, 0, 0 }}}; //Logical 1
    size_t size = 0;
    size_t num = 0;
    bool *psrc = ( bool * ) src;
    while ( size < src_size && num < wanted_num ) {
        pdest->val = *psrc ? on.val : off.val;
        size++;
        psrc++;
    }
    *translated_size = size;
    *item_num = num;
}

/*
 * Initialize the RMT Tx channel
 */
static void rmt_tx_int()
{
    rmt_config_t config;
    config.rmt_mode = RMT_MODE_TX;
    config.channel = RMT_TX_CHANNEL;
    config.gpio_num = ( gpio_num_t ) RMT_TX_GPIO;
    config.mem_block_num = 1;
    config.tx_config.loop_en = 1;
    config.tx_config.carrier_en = 0;
    config.tx_config.idle_output_en = 0;

    // clock should now be 1MHz (APB Clk is 80MHz)
    config.clk_div = 80;

    ESP_ERROR_CHECK ( rmt_config ( &config ) );
    ESP_ERROR_CHECK ( rmt_driver_install ( config.channel, 0, 0 ) );
    ESP_ERROR_CHECK ( rmt_translator_init ( config.channel, bool_to_rmt ) );
}

extern "C" void app_main()
{
    const uint16_t delaybase = 50;
    // add 100 delaybase units for white space pause
    // twice for a reversed version.
    const uint8_t pause = 100;
    const uint8_t ean_size = EAN13_width();
    const uint8_t code_size = ( ean_size + pause ) * 2;
    bool code[code_size];
    char data[10] = { '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'};

    ESP_LOGI ( BARCODE_TAG, "Configuring transmitter" );
    rmt_tx_int();

    // fill complete code with ones for white (reflected).
    for ( uint8_t i = 0; i < sizeof ( code ); i++ ) code[i] = 1;
    EAN13_build ( data, code );
    // add the code in reverse again.
    bool *pc = &code[code_size + ean_size];
    for ( uint8_t i = 0; i < EAN13_width(); i++, pc-- ) {
        *pc = code[i];
    }

    while ( 1 ) {
        ESP_ERROR_CHECK ( rmt_write_sample ( RMT_TX_CHANNEL,
                                             ( uint8_t* ) code,
                                             sizeof ( code ),
                                             true ) );
        ESP_LOGI ( BARCODE_TAG, "Barcode transmission complete" );
        vTaskDelay ( 2000 / portTICK_PERIOD_MS );
    }
    vTaskDelete ( NULL );
}
